#ifndef _OLED_H_
#define _OLED_H_

#include "stm32f10x.h"

#define	OLED_SPI_CLK  	PBout(14) //时钟线
#define OLED_SPI_MOSI 	PBout(13) //主机输出从机输入
#define OLED_SPI_RES  	PBout(12) //复位
#define OLED_SPI_DC	  	PBout(1)  //数据命令选择
#define OLED_SPI_CS		PAout(7)  //片选

#define OLED_CMD_MODE 0
#define OLED_DAT_MODE 1
#endif