#include <string.h>
#include <math.h>
#include "oled.h"
/********************************************
 功    能：OLED模拟时序发送字节(先发送MSB)
 形    参：data 要发送的数据(数据/指令)
		   mode 决定上述参数表示(数据/命令)
*********************************************/
void OLED_SPI_WriteByte(u8 data, u8 mode)
{
	u8 i;
	OLED_SPI_CS = 0;     //选中设备
	OLED_SPI_DC = mode;  //设置后续数据为命令
	
	for(i=0;i<8;i++)
	{
		OLED_SPI_CLK = 0; //为产生上升沿做准备，在上升沿到来期间修改数据线
		OLED_SPI_MOSI = (data & 0x80) ? 1 : 0;
		
		OLED_SPI_CLK = 1; //从机开始对数据线进行采样
		
		data <<= 1;//数据Bit移位
	}
	OLED_SPI_CS = 1;   //取消选中
}
u8 OLED_GRAM[8][128]; //显存,与屏幕大小一一对应
/************************************
 函数功能：画点(像素点)
 形    参：x(横坐标)：0~127
		   y(纵坐标)：0~63
           stat(点的状态)：1-亮/0-灭
 使用示例：
 OLED_DrawPoint(32, 32, 1);
*************************************/
void OLED_DrawPoint(u8 x, u8 y, u8 stat)
{
	//如果超范围,直接退出(自我保护)
	if((x>127) || (y>63)) return ;
	u8 page = y/8; //计算y所在的页数
	y %= 8;  //计算要画的点在当前字节中的位置
	if(stat)OLED_GRAM[page][x] |= 1<<y;    //对应位置写1
	else	OLED_GRAM[page][x] &= ~(1<<y); //对应位置写0
}
/********************************************
 函数功能：将屏幕缓冲区中的数据刷新到屏幕上
*********************************************/
void OLED_RefreshGram(void)
{
	u8 i,j;
	
	for(i=0;i<8;i++) 
	{
		OLED_SetCoord(0,i); //设置页坐标
		for(j=0;j<128;j++)
		{
			OLED_SPI_WriteByte(OLED_GRAM[i][j], OLED_DAT_MODE);
		}
	}
}
/****************************************
 函数功能：将图片数据写入显存
 取模方式：阴码,逐行式,低位在前,C51格式
******************************************/
void OLED_DrawImage(u8 x, u8 y, u8 w, u8 h, u8 *pImage)
{
	u8 i,j,data;
	if(w%8) w += (8-w%8); //横坐标补全
	
	u8 x0 = x; //记录x的初始位置
	for(i=0;i<(w*h/8);i++) //总共要显示的数据个数(字节)
	{
		//data = Image_27x32[i];
        data = pImage[i];
		for(j=0;j<8;j++)
		{
			OLED_DrawPoint(x, y, ((data & 0x01) ? 1 : 0)); 
			x++;   //横坐标累加
			data >>= 1;
		}	
		if((x - x0) == w) //一行的长度等于字的宽度
		{
			x = x0;    //横坐标归位
			y++;       //纵坐标+1
		}
	}
    OLED_RefreshGram();
}
