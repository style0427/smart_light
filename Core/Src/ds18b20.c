#include "ds18b20.h"

//DS18B20微秒延时函数
static void DS18B20_DelayUs(uint16_t us)
{
    HAL_Delayus(us);
}

//写DQ引脚
//bitValue(0/1)
static void DS18B20_W_DQ(uint8_t bitValue)
{
    HAL_GPIO_WritePin(GPIOB, GPIO_PIN_15, (GPIO_PinState)bitValue);
}

//读DQ引脚
static uint8_t DS18B20_R_DQ(void)
{
    return HAL_GPIO_ReadPin(GPIOB, GPIO_PIN_15);
}

//主机发送复位脉冲
static void DS18B20_ResetPulse(void)
{
    DS18B20_W_DQ(0);     //控制DQ引脚输出低电平
    DS18B20_DelayUs(500);//持续480us以上
    DS18B20_W_DQ(1);     //控制DQ引脚输出高电平---释放总线  
}

//主机检测从机存在脉冲
//返回值 0-成功/1,2-失败
static uint8_t DS18B20_PresencePulse(void)
{
    uint8_t timeout = 0;
    //1，检测从机是否拉低总线(设置超时等待100us)
    while (DS18B20_R_DQ() && timeout<=100)
    {
        DS18B20_DelayUs(1);
        timeout++;
    }
    if (timeout == 100) return 1;   //超时退出
    
    //2，检测从机是否释放总线(设置超时等待100us)
    timeout = 0;
    while (!DS18B20_R_DQ() && timeout<=240)
    {
        DS18B20_DelayUs(1);
        timeout++;
    }
    if (timeout == 240) return 2;   //超时退出
    
    return 0;
}

//初始化序列
uint8_t DS18B20_Init(void)
{
    //1，主机发送复位脉冲
    DS18B20_ResetPulse();
    //2，从机回应存在脉冲
    return DS18B20_PresencePulse(); 
}

//向DS18B20写入字节数据
static void DS18B20_WriteByte(uint8_t txd)
{
    uint8_t i;
    for (i=0; i<8;i++)
    {
        //1,产生"写时间隙"
        DS18B20_W_DQ(0);
        DS18B20_DelayUs(2); 
        //2,开始根据要写的数据(LSB先发)修改DQ引脚的电平状态
        //DS18B20_W_DQ((txd & 1<<i)?1:0);
        if (txd & 1<<0)
            DS18B20_W_DQ(1);
        else
            DS18B20_W_DQ(0);
        txd >>= 1;
        //3,单周期维持时间至少60us
        DS18B20_DelayUs(58);
        //4,周期结尾，主动释放总线（至少1us）
        DS18B20_W_DQ(1);
        DS18B20_DelayUs(2);
    }
}

//从DS18B20读取字节数据
static uint8_t DS18B20_ReadByte(void)
{    
    uint8_t i, rxd = 0;
    for (i=0; i<8; i++)
    {
        //1,主机产生"读时间隙"
        DS18B20_W_DQ(0);
        DS18B20_DelayUs(2);
        DS18B20_W_DQ(1);    //释放总线，从设备开始接管DQ引脚
        //2,读取DQ线上的电平状态（在产生读时间隙的15us内完成）
        DS18B20_DelayUs(10);
        if (DS18B20_R_DQ())
            rxd |= 1<<i;
        else 
            rxd &= ~(1<<i);
        //3,单周期维持时间至少60us
        DS18B20_DelayUs(48);
    }
    return rxd;
}    

float DS18B20_GetTemp(void)
{
    uint8_t TH,TL;
    //第一次操作：启动一次温度转换，结果保存在RAM的第1，2个字节
    DS18B20_ResetPulse();       //复位脉冲
    DS18B20_PresencePulse();    //存在脉冲
    DS18B20_WriteByte(0xCC);    //发送ROM指令
    DS18B20_WriteByte(0x44);    //发送RAM指令
    //第二次操作：
    DS18B20_ResetPulse();
    DS18B20_PresencePulse();
    DS18B20_WriteByte(0xCC);
    DS18B20_WriteByte(0xBE);    //读取RAM，默认地址是RAM第1个字节的地址
    TL = DS18B20_ReadByte();    //读取RAM第一个字节
    TH = DS18B20_ReadByte();    //读取RAM第二个字节
    //计算真实温度
    return ((TH<<8|TL)*0.0625);
}
