#include "oled.h"
#include "oledfont.h"
#include "gpio.h"

/*
    引脚定义
    名称   引脚   功能
    D0   - PB14 - SPI_SCLK
    D1   - PB13 - SPI_MOSI
    RES# - PB12 - 复位引脚(低电平有效)
    D/C# - PB1 -  数据/命令功能选择(高电平数据模式/低电平命令模式)
    CS#  - PA7 -  片选信号(低电平有效)
*/
static void OLED_W_SCLK(uint8_t _BitValue)
{
    HAL_GPIO_WritePin(GPIOB, GPIO_PIN_14, (GPIO_PinState)_BitValue);
}
static void OLED_W_MOSI(uint8_t _BitValue)
{
    HAL_GPIO_WritePin(GPIOB, GPIO_PIN_13, (GPIO_PinState)_BitValue);
}
static void OLED_W_RES(uint8_t _BitValue)
{
    HAL_GPIO_WritePin(GPIOB, GPIO_PIN_12, (GPIO_PinState)_BitValue);
}
static void OLED_W_DC(uint8_t _BitValue)
{
    HAL_GPIO_WritePin(GPIOB, GPIO_PIN_1, (GPIO_PinState)_BitValue);
}
static void OLED_W_CS(uint8_t _BitValue)
{
    HAL_GPIO_WritePin(GPIOA, GPIO_PIN_7, (GPIO_PinState)_BitValue);
}

/* 延时函数(毫秒级) */
void OLED_Delay(uint32_t _Ms)
{
    HAL_Delay(_Ms);
}

/*
*********************************************************************************************************
*	功能说明：OLED底层驱动函数-写字节
*	形    参：
*            _ucTxD  - 要写的字节
*            _Mode - 数据传输模式(0:命令模式/1:数据模式)
*	返 回 值：无
*********************************************************************************************************
*/
/* 数据传输模式定义 */
#define OLED_CMD (0) // 命令模式
#define OLED_DAT (1) // 数据模式
static void OLED_WR_Byte(uint8_t _Byte, uint8_t _Mode)
{
    uint8_t i;
    OLED_W_DC(_Mode); /* 选择数据传输模式 */
    OLED_W_CS(0);     /* 选中设备 */
    for (i = 0; i < 8; i++)
    {
        /* 双方开始修改数据 */
        OLED_W_SCLK(0);
        if (_Byte & (0x80 >> i)) /* 高位先行 */
            OLED_W_MOSI(1);
        else
            OLED_W_MOSI(0);

        /* 双方开始读取数据 */
        OLED_W_SCLK(1);
    }
    OLED_W_CS(1); /* 取消选中 */
}

/*
*********************************************************************************************************
*	功能说明：设置显示坐标(页面寻址模式,页内地址自增)
*	形    参：
*             _Page - 页地址( 0 ~ (OLED_HEIGHT/8-1) )
*             _Col  - 列地址( 0 ~ (OLED_WIDTH-1) )
*	返 回 值：无
*********************************************************************************************************
*/
static void OLED_SetPos(uint8_t _Page, uint8_t _Col)
{
    /* 通过命令 B0h 到 B7h 设置页地址 */
    OLED_WR_Byte(0xB0 + (_Page & 0x07), OLED_CMD);
    /* 通过命令 00h~0Fh 设置指针的下起始列地址(取列地址的低四位) */
    OLED_WR_Byte(0x00 + (_Col & 0x0F), OLED_CMD);
    /* 通过命令 10h~1Fh 设置指针的上起始列地址(取列地址的高四位) */
    OLED_WR_Byte(0x10 + ((_Col & 0xF0) >> 4), OLED_CMD);
}

#ifndef OLED_GRAM_MODE
///////////////////////////////////////////////////////////////////////////////////////////////////
// 常规用法
/* 清屏 */
void OLED_Cls(void)
{
    uint8_t i, j;
    for (i = 0; i < OLED_HEIGHT / 8; i++)
    {
        /* 设置页起始地址 */
        OLED_SetPos(i, 0);
        /* 连续写入一整页数据 */
        for (j = 0; j < OLED_WIDTH; j++)
            OLED_WR_Byte(0x00, OLED_DAT);
    }
}

/*
*********************************************************************************************************
*	功能说明：在屏幕上显示目标数据[核心函数]
*	形    参：
*             _Page  - 页地址(0~OLED_HEIGHT/8)
*             _Col   - 列地址(0~OLED_WIDTH)
*             _Width - 源目标的宽度
*             _Height- 源目标的高度
*             _pbuf    - 源目标的取模数据
*             _Mode  - 显示模式(1:正常模式/0:反显模式)
*	返 回 值：无
*   取模方式：阴码,列行式,逆向(低位在前),十六进制,C51格式
*********************************************************************************************************
*/
static void OLED_DispData(uint8_t _Page, uint8_t _Col, uint8_t _Width, uint8_t _Height, uint8_t *_pbuf, uint8_t _Mode)
{
    uint8_t i, j;
    uint8_t tmp;
    /* 高度补全:如果高度不是8的整数倍，将其手动补充为8的整数倍 */
    while (_Height % 8)
        _Height++;
    for (i = 0; i < _Height / 8; i++)
    {
        /* 设置起始坐标 */
        OLED_SetPos(_Page + i, _Col);
        /* 写数据 */
        for (j = 0; j < _Width; j++)
        {
            tmp = *_pbuf++;
            if (_Mode)
                OLED_WR_Byte(Byte, OLED_DAT);
            else
                OLED_WR_Byte(~Byte, OLED_DAT);
        }
    }
}

/*
*********************************************************************************************************
*	功能说明：在屏幕上显示一个字符
*	形    参：
*             _Page - 页地址(0~OLED_HEIGHT/8)
*             _Col  - 列地址(0~OLED_WIDTH)
*             _Height - 字符的高度
*             _Chr  - 字符
*             _Mode - 显示模式(1:正常模式/0:反显模式)
*	返 回 值：无
*********************************************************************************************************
*/
void OLED_DispChar(uint8_t _Page, uint8_t _Col, uint16_t _Width, uint8_t _Height, uint8_t _Chr, uint8_t _Mode)
{
    uint8_t *pbuf = NULL;
    /* 计算要显示的字符源数据在字库中的位置 */
    _Chr = _Chr - ' ';
    /* 从字库中取对应字符的取模数据 */
    switch (_Height)
    {
    case 16:
        pbuf = asc2_1608[_Chr];
        break;
    default:
        pbuf = asc2_1608[_Chr];
        break;
    }
    /* 显示数据 */
    OLED_DispData(_Page, _Col, _Width, _Height, pbuf, _Mode);
}

/*
*********************************************************************************************************
*	功能说明：在屏幕上显示一个字符串
*	形    参：
*             _Page - 页地址(0~OLED_HEIGHT/8)
*             _Col  - 列地址(0~OLED_WIDTH)
*             _Height - 字符的高度
*             _pstr   - 字符串
*             _Mode - 显示模式(1:正常模式/0:反显模式)
*	返 回 值：无
*********************************************************************************************************
*/
void OLED_DispString(uint8_t _Page, uint8_t _Col, uint8_t _Height, uint8_t *_pstr, uint8_t _Mode)
{
    uint16_t Width;
    if (_Height == 8)
        Width = 6;
    else
        Width = _Height / 2;
    while (*_pstr != '\0')
    {
        /* 自动换行 */
        if ((_Col + Width) > OLED_WIDTH)
        {
            _Page++;
            _Col = 0;
        }
        /* 过滤非法字符 */
        if (*_pstr > ' ' && *_pstr < '~')
        {
            OLED_DispChar(_Page, _Col, Width, _Height, *_pstr, _Mode);
            _Col += Width;
        }
        _pstr++;
    }
}

/*
*********************************************************************************************************
*	功能说明：在屏幕上显示一个汉字
*	形    参：
*             _Page - 页地址(0~OLED_HEIGHT/8)
*             _Col  - 列地址(0~OLED_WIDTH)
*             _Height- 汉字的高度
*             _Number- 汉字在小字库中的编号
*             _Mode  -显示模式(1:正常模式/0:反显模式)
*	返 回 值：无
*********************************************************************************************************
*/
void OLED_DispChinese(uint8_t _Page, uint8_t _Col, uint8_t _Height, uint8_t _Number, uint8_t _Mode)
{
    char *pbuf = NULL;
    /* 从字库中取数据 */
    if (_Height == 16)
        pbuf = hz16[_Number];
    else 
        return ;
    /* 数据显示 */
    OLED_DispData(_Page, _Col, _Height, _Height, pbuf, _Mode);
}

/*
*********************************************************************************************************
*	功能说明：在屏幕上显示BMP图片
*	形    参：
*             _Page  - 页地址(0~OLED_HEIGHT/8)
*             _Col   - 列地址(0~OLED_WIDTH)
*             _Width - 图片的宽度
*             _Height- 图片的高度
*             _pbuf    - 图片的取模数据
*             _Mode  - 显示模式(1:正常模式/0:反显模式)
*	返 回 值：无
*********************************************************************************************************
*/
void OLED_DispBMP(uint8_t _Page, uint8_t _Col, uint8_t _Width, uint8_t _Height, uint8_t *_pbuf, uint8_t _Mode)
{
    OLED_DispData(_Page, _Col, _Width, _Height, _pbuf, _Mode);
}

#else

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 进阶用法：构造显存(和要显示的对象等大的缓冲区[数组])
uint8_t OLED_GRAM[OLED_HEIGHT / 8][OLED_WIDTH];

/*
*********************************************************************************************************
*	功能说明：在显存中的指定位置绘制一个点
*	形    参：
*             _X     - x坐标( 0 ~ OLED_WIDTH-1 )
*             _Y     - y坐标( 0 ~ OLED_HEIGHT-1 )
*             _Color - 点的状态(1:亮/0:灭)
*	返 回 值：无
*********************************************************************************************************
*/
void OLED_DrawPoint(uint16_t _X, uint16_t _Y, uint16_t _Color)
{
    uint8_t Page = 0;
    uint8_t Col = 0;
    uint8_t Pos = 0;
    if (_X < OLED_WIDTH && _Y < OLED_HEIGHT)
    {
        Page = _Y / 8; // 计算该点所在"页(第几个一维数组)"
        Col = _X;      // 计算该点 在 所在页内的 第几个字节
        Pos = _Y % 8;  // 计算该点 在 所在字节内的 第几位
        if (_Color)
            OLED_GRAM[Page][Col] |= 1 << Pos;
        else
            OLED_GRAM[Page][Col] &= ~(1 << Pos);
    }
}

/* 复位显存 */
void OLED_ResetGram(void)
{
    uint8_t i, j;
    for (i = 0; i < OLED_HEIGHT / 8; i++)
    {
        for (j = 0; j < OLED_WIDTH; j++)
            OLED_GRAM[i][j] = 0x00;
    }
}

/* 刷新显存 */
void OLED_RefreshGram(void)
{
    uint8_t i, j;
    for (i = 0; i < OLED_HEIGHT / 8; i++)
    {
        OLED_SetPos(i, 0);
        for (j = 0; j < OLED_WIDTH; j++)
            OLED_WR_Byte(OLED_GRAM[i][j], OLED_DAT);
    }
}

/* 清屏 */
void OLED_Cls(void)
{
    OLED_ResetGram();
    OLED_RefreshGram();
}

/*
*********************************************************************************************************
*	功能说明：在显存中显示数据
*	形    参：
*             _X     - x坐标( 0 ~ OLED_WIDTH-1 )
*             _Y     - y坐标( 0 ~ OLED_HEIGHT-1 )
*             _Width - 源目标的宽度
*             _Height- 源目标的高度
*             _pbuf  - 源目标的取模数据
*             _Mode  - 显示模式(1:正常模式/0:反显模式)
*	返 回 值：无
*   取模方式：阴码,列行式,逆向(低位在前),十六进制,C51格式
*********************************************************************************************************
*/
static void OLED_DrawData(uint16_t _X, uint16_t _Y, uint16_t _Width, uint16_t _Height, uint8_t *_pbuf, uint8_t _Mode)
{
    uint8_t i, j, k;
    uint8_t tmp;

    while (_Height % 8)
        _Height++; // 高度补充为8的整数倍参与后续计算
    /* 显示所有数据 */
    for (i = 0; i < _Height / 8; i++) // 控制页切换
    {
        /* 显示(还原)一页数据 */
        for (j = 0; j < _Width; j++) // 控制列切换
        {
            /* 显示(还原)一个字节 */
            tmp = *_pbuf++; // 从取模的全数据中 依次提取字节
            if (_Mode == 0)
                tmp = ~tmp;
            for (k = 0; k < 8; k++)
            {
                /* 绘制一个像素点 */
                if (tmp & (0x01 << k)) /* 低位先行 */
                    OLED_DrawPoint(_X + j, _Y + k, 1);
                else
                    OLED_DrawPoint(_X + j, _Y + k, 0);
            }
        }
        _Y += 8; // 即将换页,更新y的起始位置
    }
}

/*
*********************************************************************************************************
*	功能说明：在显存中显示字符
*	形    参：
*             _X     - x坐标( 0 ~ OLED_WIDTH-1 )
*             _Y     - y坐标( 0 ~ OLED_HEIGHT-1 )
*             _Width - 字符的宽度
*             _Height- 字符的高度
*             _Chr  - 字符的取模数据
*             _Mode  - 显示模式(1:正常模式/0:反显模式)
*	返 回 值：无
*********************************************************************************************************
*/
void OLED_DrawChar(uint16_t _X, uint16_t _Y, uint16_t _Width, uint16_t _Height, uint8_t _Chr, uint8_t _Mode)
{
    uint8_t *pbuf = NULL;
    _Chr = _Chr - ' ';
    switch (_Height)
    {
    case 8:
        pbuf = asc2_0806[_Chr];
        break;
    case 16:
        pbuf = asc2_1608[_Chr];
        break;
    }
    OLED_DrawData(_X, _Y, _Width, _Height, pbuf, _Mode);
}

/*
*********************************************************************************************************
*	功能说明：在显存中显示字符串
*	形    参：
*             _X     - x坐标( 0 ~ OLED_WIDTH-1 )
*             _Y     - y坐标( 0 ~ OLED_HEIGHT-1 )
*             _Height- 字符的高度
*             _pstr    - 要显示的字符串
*             _Mode  - 显示模式(1:正常模式/0:反显模式)
*	返 回 值：无
*********************************************************************************************************
*/
void OLED_DrawString(uint16_t _X, uint16_t _Y, uint16_t _Height, char *_pstr, uint8_t _Mode)
{
    uint16_t Width;
    if (_Height == 8)
        Width = 6;
    else
        Width = _Height / 2;
    while (*_pstr != '\0')
    {
        /* 自动换行 */
        if ((_X + Width) > OLED_WIDTH)
        {
            _Y += _Height;
            _X = 0;
        }
        /* 过滤非法字符 */
        if (*_pstr >= ' ' && *_pstr <= '~')
        {
            OLED_DrawChar(_X, _Y, Width, _Height, *_pstr, _Mode);
            _X += Width;
        }
        _pstr++;
    }
}

/*
*********************************************************************************************************
*	功能说明：在显存中显示汉字
*	形    参：
*             _X     - x坐标( 0 ~ OLED_WIDTH-1 )
*             _Y     - y坐标( 0 ~ OLED_HEIGHT-1 )
*             _Height- 汉字的高度
*             _Number- 汉字在小字库中的编号
*             _Mode  - 显示模式(1:正常模式/0:反显模式)
*	返 回 值：无
*********************************************************************************************************
*/
void OLED_DrawChinese(uint16_t _X, uint16_t _Y, uint16_t _Height, uint8_t _Number, uint8_t _Mode)
{
    uint8_t *pbuf = NULL;
    /* 从字库中取数据 */
    if (_Height == 16)
        pbuf = hz16[_Number];
    else if (_Height == 12)
        pbuf = hz12[_Number];
    else  return;
    /* 数据显示 */
    OLED_DrawData(_X, _Y, _Height, _Height, pbuf, _Mode);
}

/*
*********************************************************************************************************
*	功能说明：在显存中显示BMP图片
*	形    参：
*             _X     - x坐标( 0 ~ OLED_WIDTH-1 )
*             _Y     - y坐标( 0 ~ OLED_HEIGHT-1 )
*             _Width - 图片的宽度
*             _Height- 图片的高度
*             _pbuf    - 图片的取模数据
*             _Mode  - 显示模式(1:正常模式/0:反显模式)
*	返 回 值：无
*********************************************************************************************************
*/
void OLED_DrawBMP(uint16_t _X, uint16_t _Y, uint16_t _Width, uint16_t _Height, uint8_t *_pbuf, uint8_t _Mode)
{
    OLED_DrawData(_X, _Y, _Width, _Height, _pbuf, _Mode);
}

/*
*********************************************************************************************************
*	功能说明：绘制直线
*	形    参：
*             (x1,y1) - 起点坐标
*             (x2,y2) - 终点坐标
*             c       - 点的颜色
*	返 回 值：无
*********************************************************************************************************
*/
void OLED_DrawLine(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2, uint16_t c)
{
    uint16_t t;
    int xerr = 0, yerr = 0, delta_x, delta_y, distance;
    int incx, incy, uRow, uCol;
    delta_x = x2 - x1; // 计算坐标增量
    delta_y = y2 - y1;
    uRow = x1;
    uCol = y1;
    if (delta_x > 0)
        incx = 1; // 设置单步方向
    else if (delta_x == 0)
        incx = 0; // 垂直线
    else
    {
        incx = -1;
        delta_x = -delta_x;
    }
    if (delta_y > 0)
        incy = 1;
    else if (delta_y == 0)
        incy = 0; // 水平线
    else
    {
        incy = -1;
        delta_y = -delta_y;
    }
    if (delta_x > delta_y)
        distance = delta_x; // 选取基本增量坐标轴
    else
        distance = delta_y;
    for (t = 0; t <= distance + 1; t++) // 画线输出
    {
        OLED_DrawPoint(uRow, uCol, c); // 画点
        xerr += delta_x;
        yerr += delta_y;
        if (xerr > distance)
        {
            xerr -= distance;
            uRow += incx;
        }
        if (yerr > distance)
        {
            yerr -= distance;
            uCol += incy;
        }
    }
}

/*
*********************************************************************************************************
*	功能说明：绘制角度直线
*	形    参：
*             (x,y) - 原点坐标
*             angle - 角度(3点钟方向为0°,角度为顺时针递增)
*             start - 起始点距离原点的距离(从起始点向圆心开始绘制直线)
*             len   - 直线的长度
*	返 回 值：无
*********************************************************************************************************
*/
#include <math.h>
void OLED_DrawAngleLine(uint16_t x, uint16_t y, float angle, uint32_t start, uint32_t len, uint16_t c)
{
    int i;
    int x0, y0;
    float k = angle * (3.1415926535 / 180.0);
    for (i = start - len; i < start; i++)
    {
        x0 = cos(k) * i;
        y0 = sin(k) * i;
        OLED_DrawPoint(x + x0, y + y0, c);
    }
}

/*
*********************************************************************************************************
*	功能说明：绘制圆
*	形    参：
*             (x0,y0) - 圆心坐标
*             r       - 半径
*             c       - 点的颜色
*	返 回 值：无
*********************************************************************************************************
*/
void OLED_DrawCircle(uint16_t x0, uint16_t y0, uint8_t r, uint16_t c)
{
    int a, b;
    int di;
    a = 0;
    b = r;
    di = 3 - (r << 1); // 判断下个点位置的标志
    while (a <= b)
    {
        OLED_DrawPoint(x0 + a, y0 - b, c); // 5
        OLED_DrawPoint(x0 + b, y0 - a, c); // 0
        OLED_DrawPoint(x0 + b, y0 + a, c); // 4
        OLED_DrawPoint(x0 + a, y0 + b, c); // 6
        OLED_DrawPoint(x0 - a, y0 + b, c); // 1
        OLED_DrawPoint(x0 - b, y0 + a, c);
        OLED_DrawPoint(x0 - a, y0 - b, c); // 2
        OLED_DrawPoint(x0 - b, y0 - a, c); // 7
        a++;
        // Bresenham画圆算法
        if (di < 0)
            di += 4 * a + 6;
        else
        {
            di += 10 + 4 * (a - b);
            b--;
        }
    }
}

/*
*********************************************************************************************************
*	功能说明：绘制矩形
*	形    参：
*             (sx,sy) - 起点坐标(左上角)
*             (ex,ey) - 终点坐标(右下角)
*             c       - 点的颜色
*	返 回 值：无
*********************************************************************************************************
*/
void OLED_DrawRect(uint16_t sx, uint16_t sy, uint16_t ex, uint16_t ey, uint16_t c)
{
    OLED_DrawLine(sx, sy, ex, sy, c);
    OLED_DrawLine(sx, sy, sx, ey, c);
    OLED_DrawLine(sx, ey, ex, ey, c);
    OLED_DrawLine(ex, sy, ex, ey, c);
}

/*
*********************************************************************************************************
*	功能说明：填充矩形
*	形    参：
*             (sx,sy) - 起点坐标(左上角)
*             (ex,ey) - 终点坐标(右下角)
*             c       - 点的颜色
*	返 回 值：无
*********************************************************************************************************
*/
void OLED_FillRect(uint16_t sx, uint16_t sy, uint16_t ex, uint16_t ey, uint16_t c)
{
    uint16_t i, j;
    for (i = sx; i < ex; i++)
    {
        for (j = sy; j < ey; j++)
        {
            OLED_DrawPoint(i, j, c); // 设置光标位置
        }
    }
}

#endif

/* OLED显示屏初始化 */
void OLED_Init(void)
{
    /* 引脚初始化 */
    //...
    /* SSD1306硬件复位 */
    OLED_W_RES(0);
    OLED_Delay(100);
    OLED_W_RES(1);
    /* SSD1306初始化配置 */
    OLED_WR_Byte(0xAE, OLED_CMD); //--关闭显示
    OLED_WR_Byte(0x00, OLED_CMD); //--set low column address
    OLED_WR_Byte(0x10, OLED_CMD); //--set high column address
    OLED_WR_Byte(0x40, OLED_CMD); //--set start line address
    OLED_WR_Byte(0x81, OLED_CMD); //--设置屏幕对比度寄存器寄存器
    OLED_WR_Byte(0xCF, OLED_CMD); //--设置对比度[0x00~0xFF]
    OLED_WR_Byte(0xA1, OLED_CMD); //--Set SEG/Column Mapping 0xa0 左右反置 0xa1 正常
    OLED_WR_Byte(0xC8, OLED_CMD); //--Set COM/Row Scan Direction 0xc0 上下反置 0xc8 正常
    OLED_WR_Byte(0xA6, OLED_CMD); //--set normal display
    OLED_WR_Byte(0xA8, OLED_CMD); //--set multiplex ratio(1 to 64)
    OLED_WR_Byte(0x3F, OLED_CMD); //--1/64 duty
    OLED_WR_Byte(0xD3, OLED_CMD); //--set display offset
    OLED_WR_Byte(0x00, OLED_CMD); //--not offset
    OLED_WR_Byte(0xD5, OLED_CMD); //--set display clock divide ratio/oscillatorfrequency
    OLED_WR_Byte(0x80, OLED_CMD); //--set divide ratio, Set Clock as 100 Frames/Sec
    OLED_WR_Byte(0xD9, OLED_CMD); //--set pre-charge period
    OLED_WR_Byte(0xF1, OLED_CMD); //--Set Pre-Charge as 15 Clocks & Discharge as 1 Clock
    OLED_WR_Byte(0xDA, OLED_CMD); //--set com pins hardware configuration
    OLED_WR_Byte(0x12, OLED_CMD);
    OLED_WR_Byte(0xDB, OLED_CMD); //--set vcomh
    OLED_WR_Byte(0x40, OLED_CMD); //--Set VCOM Deselect Level
    OLED_WR_Byte(0x20, OLED_CMD); //--设置寻址方式 (0x00/0x01/0x02)
    OLED_WR_Byte(0x02, OLED_CMD); //--页面寻址模式(页内列地址自增)
    OLED_WR_Byte(0x8D, OLED_CMD); //--set Charge Pump enable/disable
    OLED_WR_Byte(0x14, OLED_CMD); //--set(0x10) disable
    OLED_WR_Byte(0xA4, OLED_CMD); //--Disable Entire Display On (0xa4/0xa5)
    OLED_WR_Byte(0xA6, OLED_CMD); //--设置显示模式(0Xa6 正常模式亮为 1/0xa7 反显模式亮为 0)
    OLED_WR_Byte(0xAF, OLED_CMD); //--开启显示
    /* 清屏 */
    OLED_Cls();
}
