#include "esp8266.h"
#include "usart.h"
#include <string.h>
#include <stdio.h>
uint8_t vlan_flag = 0;
static void ESP8266_DelayMs(uint32_t ms)
{
    HAL_Delay(ms);
}

static void ESP8266_SendString(char *pstr)
{
    HAL_UART_Transmit(&huart3, (uint8_t *)pstr, strlen(pstr), 0xFFFF);
}

//核心代码：通过串口3发送AT指令
//要求：(1)能自主判断指令是否发送成功
//      (2)在指令发送失败是自动重发（最多5次）
static uint8_t ESP8266_SendAtCmd(char *cmd, char *ack)
{
    uint16_t i, j;
    for (j=0; j<5; j++) //---最多发送5次
    {
        //主机通过串口3发送AT指令
        g_Usart3RxCnt = 0;
        g_Usart3RxFlag = 0;
        ESP8266_SendString(cmd);    
        //等待WIFI响应,持续判断 --- 设定超时自动退出（3s）
        for (i=0; i<1000*3; i++)
        {
            //1234   5678  988732  OK
            if (g_Usart3RxFlag)  //---成功接收到响应
            {
                g_Usart3RxFlag = 0;
                //printf("%s\n", g_Uart3RxBuf); //调试
                //---判断响应是否正确，把接收的响应和ack做比较
                if (strstr((char *)g_Usart3RxBuf, ack))
                {
                    g_Usart3RxCnt = 0;
                    printf("[OK]\n");
                    return 0;
                }
            }
            ESP8266_DelayMs(1);
        }
    }
    g_Usart3RxCnt = 0;
    printf("[ERROR]\n");
    return 1;
}

//STA模式（客户端模式）初始化
uint8_t ESP8266_STA_TCPClientInit(ESP8266_InfoTypeDef espInfo)
{
    char buf[50];
    printf("\nWIFI模块初始化...\n");
    printf("[1/7]退出透传....................");
    ESP8266_SendString("+++");
    ESP8266_DelayMs(1000);
    if (ESP8266_SendAtCmd("AT\r\n", "OK"))             {LED(3,0);vlan_flag = 0;return 1;}
    
    printf("[2/7]设置WIFI为STA模式...........");
    if (ESP8266_SendAtCmd("AT+CWMODE=1\r\n", "OK"))    {LED(3,0);vlan_flag = 0;return 2;}
    
    printf("[3/7]连接AP热点..................");
    sprintf(buf, "AT+CWJAP_DEF=\"%s\",\"%s\"\r\n", espInfo.ssid, espInfo.pwd); /* 动态拼接关键信息 */
    if (ESP8266_SendAtCmd(buf, "OK"))                  {LED(3,0);vlan_flag = 0;return 3;}
    
    printf("[4/7]开启单连接..................");
    if (ESP8266_SendAtCmd("AT+CIPMUX=0\r\n", "OK"))    {LED(3,0);vlan_flag = 0;return 4;}
    
    printf("[5/7]连接服务器..................");
    sprintf(buf, "AT+CIPSTART=\"TCP\",\"%s\",%d\r\n", espInfo.ip, espInfo.port);
    if (ESP8266_SendAtCmd(buf, "OK"))                  {LED(3,0);vlan_flag = 0;return 5;}
    
    printf("[6/7]设置透传模式................");
    if (ESP8266_SendAtCmd("AT+CIPMODE=1\r\n", "OK"))   {LED(3,0);vlan_flag = 0;return 6;}
    
    printf("[7/7]开始透传...................."); /* 此后与服务器保持长连接,断开自动重连 */  
    if (ESP8266_SendAtCmd("AT+CIPSEND\r\n", ">"))      {LED(3,0);return 7;}
    
    /* 显示相关信息,等待设备连接 */
    printf("**************************\n");
    printf("热点名称：%s\n", espInfo.ssid);
    printf("热点密码：%s\n", espInfo.pwd);
    printf("服务器ip地址：%s\n", espInfo.ip);
    printf("服务器端口号：%d\n", espInfo.port);
    printf("**************************\n");
    vlan_flag = 1;
    LED(2,1);//连接成功提示
    return 0;
}
uint8_t ESP8266_ReqRes_DataParsing(char *reqRes, char *keywords, char *result)
{
    char *p1 = NULL;
    char *p2 = NULL;
    if(strstr(reqRes, "last_update") != NULL) //判断请求响应的数据是否正确
    {
        p1 = strstr(reqRes, keywords); //查找关键词
        if(p1 != NULL)
        {
            p1 += strlen(keywords) + 3;
            p2 = strstr(p1, "\""); //查找末端的 "
            strncpy(result, p1, p2 - p1); //拷贝数据
            return (p2-p1);
        }
    }
return 1;
}
