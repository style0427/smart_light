/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "adc.h"
#include "rtc.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <stdio.h>
#include <string.h>
#include "esp8266.h"
#include "ds18b20.h"
#include "oled.h"
#include "sty0427.h"
#include "mqtt.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */
#if 1
//热点的名称及密码
#define WIFI_NAME       "Style_Cool_WLAN"  //#热点密码
#define WIFI_PWD        "2002427gy"    //#热点密码

//默认域名(DNS)格式：{ProductID}.iotcloud.tencentdevices.com
#define TencentCloud_ServerIp    "106.55.124.154"   //#云服务器ip地址[固定值]  
#define TencentCloud_ServerPort  1883               //#云服务器端口号[固定值]

//物联网平台上的"设备"信息(主要用于生成下述MQTT连接所使用的相关信息)
#define ProductID       "L4P638SOXE"                //#产品ID
#define DeviceName      "LED_834"                      //#设备名称
#define DeviceSceret    "p7EzCbUzTAbFcmdve6QauA=="  //#设备密钥[定期自动更新] - [最后更新时间:2023-04-06 17:16:43]

//通过MQTT协议连接腾讯云物联网云平台所需关键信息[随着设备密钥的更新而更新] - [最后更新时间:2023-04-06]
#define UserName    "L4P638SOXELED_834;12010126;ZFKUM;1686626432"                                    //#用户名称,格式固定：由专门的软件生成
#define Password    "92a213fc005774abff3714bedf4a0d4080f913508089d5134c7a1807c450de96;hmacsha256" //#密码,格式固定：由专门的软件生成

//通过MQTT协议发布/订阅信息所使用的主题(可以理解为一个特定的标签,由物联网平台提供)
//(腾讯云物联网平台,服务端)
/*
| Topic权限（腾讯云物联网开发平台）              | 操作权限 | 备注                     |
| --------------------------------------------- | -------- | ----------------------- |
| $thing/up/property/{ProductID}/{DeviceName}   | 发布     | 属性上报                |
| $thing/down/property/{ProductID}/{DeviceName} | 订阅     | 属性下发与属性上报响应  |

| Topic权限（腾讯连连微信小程序）               | 操作权限 | 备注                     |
| -------------------------------------------- | -------  | ----------------------- |
| $thing/up/service/{ProductID}/{DeviceName}   | 发布     | 属性上报                |
| $thing/down/service/{ProductID}/{DeviceName} | 订阅     | 属性下发与属性上报响应  |
*/
char message[200];      //#消息
char ClientID[50];      //#客户端ID,格式固定：{ProductID}{DeviceName}
char TencentCloud_PublishTopic[100];     //#腾讯云平台发布主题
char TencentCloud_SubscribeTopic[100];   //#腾讯云平台订阅主题
char TencentApp_SubscribeTopic[100];     //#腾讯连连订阅主题
#endif
/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
extern unsigned char ball_30x30[]; 
extern unsigned char stone26x26[];
extern unsigned char halfstone13x26[];
extern unsigned char source7x7[];
extern unsigned char pause67[];
extern unsigned char start67[];
extern unsigned char led9x9[];
extern unsigned char scan1111[];
extern uint8_t vlan_flag;
extern uint8_t ikunnum;
#define ikunmax 34
uint8_t tmp_flag = 0;     //按键
uint8_t led_flag = 0;     //路灯状态标志位
uint8_t led_flag1 = 0;     //路灯状态标志位
uint16_t ledmax = 0;
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
//按键检测--检测结果通过返回值返回
//目标：实现按一次识别一次
//标记：记录按键状态
//使用static修饰局部变量，延长使用周期同main函数
uint8_t KEY_Scan(void)
{
    static uint8_t flag = 1;   //1--按键松开
    if((KEY_1 || !KEY_2 || !KEY_3 || !KEY_4)&&flag)
    {
        HAL_Delay(15);  //延时消抖
        flag = 0;   //0--按键闭合
        if(KEY_1)   return 1;
        else if(!KEY_2) return 2;
        else if(!KEY_3) return 3;
        else if(!KEY_4) return 4;
    }
    else if(!KEY_1 && KEY_2 && KEY_3 && KEY_4)
    {
        flag = 1;   //按键松开，准备下一次检测
    }
    return 0;
}
void ADC_Print(const uint16_t advalue,const float analog)
{
    #if 1
    //直接输出
    printf("AD值:%d\t电压:%.2fv\t",advalue,analog);//输出AD电压值
    #else
    //图形表达
    for(uint8_t i=0;i<(advalue/100);i++)
    {
        printf("_");
    }
    printf("\n");
    #endif
}
//微秒延时
void HAL_Delayus(uint16_t us)
{
    __HAL_TIM_SET_COUNTER(&htim2,0);        //计数器清零
    HAL_TIM_Base_Start(&htim2);             //启动计数器
    while(__HAL_TIM_GET_COUNTER(&htim2)<us) //获取当前时间
    {}
    HAL_TIM_Base_Stop(&htim2);              //停止计数器
}
void OLED_time(char *prtchar)
{
  HAL_RTC_GetDate(&hrtc,&sDate,RTC_FORMAT_BIN);//获取日期
  HAL_RTC_GetTime(&hrtc,&sTime,RTC_FORMAT_BIN);//获取时间
  //时间显示
  OLED_DrawBMP(107, 3, 17, 7, (uint8_t *)source7x7, 1);
  sprintf(prtchar,"%04d%02d%02d",2000+sDate.Year,sDate.Month,sDate.Date);//获取
  OLED_DrawString(3, 3, 8, prtchar, 1);//打印
  sprintf(prtchar,"%02d:%02d:%02d",sTime.Hours,sTime.Minutes,sTime.Seconds);//获取
  OLED_DrawString(54, 3, 8, prtchar, 1);//打印
}
void OLED_Get()
{
    OLED_KEY();
    OLED_LINE();
    OLED_RefreshGram();
    while(!KEY_Scan()){};
}
void Time_CHARGE(char *buff,uint16_t cnt,char *prtchar)
{
    if (buff[0] == '*' && cnt == 15)
        {
            sDate.Year  = (buff[3]-'0')*10 + buff[4]-'0';
            sDate.Month = (buff[5]-'0')*10 + buff[6]-'0';
            sDate.Date  = (buff[7]-'0')*10 + buff[8]-'0';
            HAL_RTC_SetDate(&hrtc, &sDate, RTC_FORMAT_BIN);
            sTime.Hours   = (buff[9]-'0')*10  + buff[10]-'0';
            sTime.Minutes = (buff[11]-'0')*10 + buff[12]-'0';
            sTime.Seconds = (buff[13]-'0')*10 + buff[14]-'0';
            HAL_RTC_SetTime(&hrtc, &sTime, RTC_FORMAT_BIN);
            OLED_ResetGram();
            sprintf(prtchar,"Time Calibration");
            OLED_DrawString(15,20,8,prtchar,1);
            sprintf(prtchar,"OVER");
            OLED_DrawString(49,27,16,prtchar,1);
            printf("/*系统时间校准完毕*/\n");
            OLED_DrawRect(10,16,117,44,1);
            OLED_Get();
        } 
}
void Cloudconnect(ESP8266_InfoTypeDef espInfo)
{
    printf("【连接】与腾讯云服务器建立TCP连接.................\n");
    while (ESP8266_STA_TCPClientInit(espInfo));
    /**************************连接云服务器上的物联网开发平台**************************/
    printf("【连接】与腾讯云物联网开发平台建立MQTT连接........\n");
    sprintf(ClientID, "%s%s", ProductID, DeviceName);
    while (MQTT_Connect(ClientID, UserName, Password));
    /**********************************主动订阅相关主题********************************/
    /* 动态拼接，得到相关主题 */
    sprintf(TencentCloud_PublishTopic, "$thing/up/property/%s/%s", ProductID, DeviceName);
    sprintf(TencentCloud_SubscribeTopic, "$thing/down/property/%s/%s", ProductID, DeviceName);
    sprintf(TencentApp_SubscribeTopic, "$thing/down/service/%s/%s", ProductID, DeviceName);
    /* 订阅 */
    printf("【订阅】\"腾讯云物联网开发平台\"主题................");
    MQTT_Subscribe(TencentCloud_SubscribeTopic, 1, 0);
    printf("【订阅】\"腾讯连连微信小程序\"主题..................");
    MQTT_Subscribe(TencentApp_SubscribeTopic, 1, 0);
}
/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
  uint8_t keyval = 0;       //键值
  uint16_t time = 5; 
  uint16_t tim = 30;
  uint16_t timemax = 5;     //ADC计数用
  uint16_t advalue;
  uint8_t time_flag = 1;    //定时器启动标志
  uint8_t timers = 0;       
  uint8_t timerm = 0;       
  uint8_t timerh = 0;       
  uint8_t timer1 = 0;       //定时器用变量
  uint8_t page = 5;         //页面变量
  float analog;
  uint8_t tmp1;
  uint8_t tmp3;
  ledmax = 3000;
  char URL[] = "GET https://api.seniverse.com/v3/weather/now.json?key=S1TxYz1KQJxZwZuVd&location=nanchang&language=en&unit=c\n";
  ESP8266_InfoTypeDef espInfo1 = {"Style_Cool_WLAN","2002427gy","api.seniverse.com",80};
  char prtchar[]="wait....";
  char timechar[256];
  char weatherchar[]="no information";
  uint16_t prtcnt;
  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */
  //ESP8266_STA_TCPClientInit(espInfo);//wifi初始化
  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_USART1_UART_Init();
  MX_USART3_UART_Init();
  MX_TIM1_Init();
  MX_TIM3_Init();
  MX_RTC_Init();
  MX_ADC1_Init();
  MX_TIM2_Init();
  /* USER CODE BEGIN 2 */
  //OLED显示屏初始化
  OLED_Init();
  OLED_DrawString(27, 46, 16, "loading...", 1);
  OLED_DrawBMP(50, 18, 13, 26, (uint8_t *)halfstone13x26, 1);//半岩
  OLED_RefreshGram();
  HAL_UART_Receive_IT(&huart1,&tmp1, 1); //使能中断 ---"接收缓冲区非空中断"
  HAL_UART_Receive_IT(&huart3,&tmp3, 1); //使能中断 ---"接收缓冲区非空中断"
  uint8_t tmp = DS18B20_Init();
  printf("/*温度传感模块初始化成功,tmp=%d*/\n", tmp);
  /***********************************连接云服务器***********************************/
  ESP8266_InfoTypeDef espInfo = {WIFI_NAME, WIFI_PWD, TencentCloud_ServerIp, TencentCloud_ServerPort};
  HAL_Delay(3000);
  Cloudconnect(espInfo);
  //OLED启动成功界面 
  OLED_ResetGram();
  OLED_DrawBMP(50, 18, 26, 26, (uint8_t *)stone26x26, 1);//整岩
  OLED_MY();
  OLED_Get();
  page = 0;
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
    if(page == 1){timemax = 5;}
    else{timemax = 35;}
    if(time >= timemax)
    {
        /*ADC采集*/
        HAL_ADC_Start(&hadc1);//启动
        HAL_ADC_PollForConversion(&hadc1,10);//阻塞等待转换完成，最长10ms
        advalue = HAL_ADC_GetValue(&hadc1);//读取转换后AD值
        analog = advalue * (3.3/4096.0);//转换为电压
        if(tim == 30)
        {
            sprintf(message, "{\"method\":\"report\",\"clientToken\":\"wbyq\",\"params\":{\"UPLOAD_0\":%.1f,\"UPLOAD_1\":%d}}", DS18B20_GetTemp(),advalue);
            printf("【上行】%s\n", message);
            MQTT_Publish(TencentCloud_PublishTopic, message, 0);
            g_Usart3RxCnt = 0;
            g_Usart3RxFlag = 0;
            tim = 0;
        }
        time++;
        HAL_GPIO_TogglePin(GPIOB,GPIO_PIN_6);//翻转
        time = 0;
    }
    keyval = KEY_Scan();
    if(keyval)
    {     
//        BEEP(0);
        HAL_Delay(10);
//        BEEP(1); 
        switch(keyval)
        {
            case 1: if(page == 0){page = 1;}//界面1 ikun定时器
                    else if(page == 1)//计数器复位+结束
                    {
                        timerh = 0;
                        timerm = 0;
                        timers = 0;
                        time_flag=1;
                    }
                    else if(page == 2){page = 4;}//WiFi连接设置页
                    else if(page == 4)//设置确认键
                    {   
                        OLED_ResetGram();
                        OLED_DrawString (21,26,8,"Please Waiting...",1);
                        OLED_DrawBMP(57,40, 11, 11, (uint8_t *)scan1111, 1);
                        OLED_RefreshGram();
                        switch(tmp_flag)
                        {
                            case 0:ESP8266_STA_TCPClientInit(espInfo1);//心知天气初始化
                                   HAL_UART_Transmit(&huart3, (uint8_t *)URL, strlen(URL), 0xFFFF);
                                   break;
                            case 1:HAL_Delay(3000);//腾讯云连接
                                   Cloudconnect(espInfo);
                                   break;
                            case 2:
                                   break;//上传
                            case 3:
                                   break;//下载
                        }
                        OLED_WAIT("OVER",16);
                        page = 2;
                    }
                    else if(page == 3)//确认键
                    {
                        if(vlan_flag)
                        {
                        switch(tmp_flag)//WIFI获取界面判断
                        {
                            case 0:HAL_UART_Transmit(&huart3, (uint8_t *)URL, strlen(URL), 0xFFFF);
                                   ESP8266_ReqRes_DataParsing((char *)g_Usart3RxBuf,"text", prtchar);
                                   break;
                            case 1:HAL_UART_Transmit(&huart3, (uint8_t *)URL, strlen(URL), 0xFFFF);
                                   OLED_ResetGram();
                                   OLED_DrawString (21,26,8,"Please Waiting...",1);
                                   OLED_DrawBMP(57,40, 11, 11, (uint8_t *)scan1111, 1);
                                   HAL_Delay(50);
                                   OLED_Get();
                                   OLED_ResetGram();
                                   //ESP8266_ReqRes_DataParsing((char *)g_Usart3RxBuf,"name", prtchar);
                                   OLED_DrawString (32,6,16,"Nanchang",1);
                                   memset(prtchar,0,strlen(prtchar));
                                   prtcnt = ESP8266_ReqRes_DataParsing((char *)g_Usart3RxBuf,"text", weatherchar);
                                   HAL_Delay(100);
                                   printf("%s",weatherchar);
                                   for(int i=0;i<prtcnt;i++)
                                   {
                                       prtchar[i]=weatherchar[i];
                                   }
                                   OLED_DrawString (28,28,8,prtchar,1);
                                   OLED_Get();
                                   break;
                            case 2:sprintf(message, "{\"method\":\"report\",\"clientToken\":\"wbyq\",\"params\":{\"UPLOAD_0\":%.1f,\"UPLOAD_1\":%d}}", DS18B20_GetTemp(),advalue);
                                   printf("【上行】%s\n", message);
                                   MQTT_Publish(TencentCloud_PublishTopic, message, 0);
                                   g_Usart3RxCnt = 0;
                                   g_Usart3RxFlag = 0;
                                   OLED_WAIT("OK",16);
                                   break;//上传信息
                            case 3:
                                   break;//下载命令
                        }
                        printf("%s\n",prtchar);
//                        OLED_DrawString(16, 0, 16, prtchar, 1);
                        }
                        else{OLED_WAIT("WIFI can't use!",8);}
                    }
                    break;
            case 2: if(page == 0)//界面2 温度/AD显示
                    {
                        page = 2;
                        ADC_Print(advalue,analog);
                        printf("当前温度:%.2f℃\n", DS18B20_GetTemp());
                    }
                    else if(page == 1)//计数器开始/暂停
                    {
                        time_flag=!time_flag;
                        timer1 = sTime.Seconds;
                    }
                    else if(page == 2)//
                    {
                        
                    }
                    else if((page == 3)||(page == 4))//左移按键
                    {
                        if(tmp_flag ==0){tmp_flag = 3;}
                        else{tmp_flag--;}//向左
                    }
                    break;         
            case 3: if(page == 0)//界面3 WiFi云平台获取
                    {
                        page = 3;
                    }
                    else if(page == 2)//
                    {
                        
                    }
                    else if((page == 3)||(page == 4))//右移按键
                    {
                        if(tmp_flag ==3){tmp_flag = 0;}
                        else{tmp_flag++;}//向右
                    }
                    break;
            case 4:page = 0;break;//返回主界面
        }        
    }
    OLED_ResetGram();
    OLED_ICON();
    if((page!=3)&&(page!=4)){tmp_flag = 0;}
    switch(page)//页面判断
    {
        case 0:
               break;
        case 1:if(!time_flag){OLED_DrawBMP(5, 30, 6, 7, (uint8_t *)pause67, 1);}//pause
               else{OLED_DrawBMP(5, 30, 7, 7, (uint8_t *)start67, 1);}//start
               sprintf(prtchar,"%02d:%02d:%02d",timerh,timerm,timers);//获取
               OLED_DrawString(1, 50, 8, prtchar, 1);//打印
               break;
        case 2:sprintf(prtchar,"AD:%d",advalue);
               OLED_DrawString(54, 12, 16, prtchar, 1);
               sprintf(prtchar,"temp:%.2f", DS18B20_GetTemp());
               OLED_DrawString(41, 28, 16, prtchar, 1);
               break;
        case 3:OLED_Chose(tmp_flag);
               //printf("%d\n",tmp_flag);
               break;
        case 4:OLED_Chose(tmp_flag);
               break;
    }
    OLED_PAGE(page);
    if(g_Usart1RxFlag)
    {
      //数据处理：把字符数组转换为字符串
      printf("串口1收到长度%d,内容:%s\n",g_Usart1RxCnt,g_Usart1RxBuf);
      HAL_UART_Transmit(&huart3, g_Usart1RxBuf,g_Usart1RxCnt, 0xFFFF);
      //时间校准
      Time_CHARGE((char *)g_Usart1RxBuf,g_Usart1RxCnt,prtchar);
      //物联网模拟 蜂鸣器BEEP(x),灯LED(x,y)
       if(strstr((char *)g_Usart1RxBuf,"BEEP_"))
       {
          if(strstr((char *)g_Usart1RxBuf,"ON"))
          {
              BEEP(0);
          }
          else if(strstr((char *)g_Usart1RxBuf,"OFF"))
          {
              BEEP(1);
          }
       }
       else if(strstr((char *)g_Usart1RxBuf,"LED"))
       {
          int num = g_Usart1RxBuf[3]-48;//ascll码转换
          if(strstr((char *)g_Usart1RxBuf,"ON")!= NULL)
          {
              LED(num,1);
          }
          else if(strstr((char *)g_Usart1RxBuf,"OFF"))
          {
              LED(num,0);
          }
       }
      g_Usart1RxFlag = 0;
      g_Usart1RxCnt = 0;
    }
    if(g_Usart3RxFlag)
    {
      //数据处理：把字符数组转换为字符串
      printf("串口3收到长度%d,内容:%s\n",g_Usart3RxCnt,g_Usart3RxBuf);
      /* 从腾讯云物联网开发平台下发的数据 */
      /* 此部分数据中夹杂着很多'\0',所以需要先对接收到的原始数据进行一次过滤筛选 */
      uint16_t i, j = 0;
      for (i = 0; i < g_Usart3RxCnt; i++)
      {
          if (g_Usart3RxBuf[i] != '\0')
              {
                  message[j] = g_Usart3RxBuf[i];
                  j++;
              }
      }
      message[j] = '\0';
      printf("【下行】%s\n", message);/*
      "params":{"LED1":1,"LED2":1,"BEEP":1,"DS18B20_0":0,"DS18B20_1":30}}
      {"BEEP":1,"DS18B20_1":0,"LED1":1,"LED2":1}}
      */
      /* 控制应用 */char *p1 = NULL;
      if (strstr(message, "control"))
          {
              p1 = strstr(message, "BEEP");
              if (p1)
              {
                  if ((*(p1 + 6) - '0') == 1)
                  {
                      BEEP(0);
                  }
                  else
                  {
                      BEEP(1);
                  }
                }
                p1 = strstr(message, "LED1");
                if (p1)
                {
                    if ((*(p1 + 6) - '0') == 1)
                    {
                        LED(3,1);
                        led_flag = 1;
                        sprintf(message, "{\"method\":\"report\",\"clientToken\":\"wbyq\",\"params\":{\"LEDT0\":1}}");
                        printf("【上行】%s\n", message);
                        MQTT_Publish(TencentCloud_PublishTopic, message, 0);
                        g_Usart3RxCnt = 0;
                        g_Usart3RxFlag = 0;
                    }
                    else
                    {
                        LED(3,0);
                        led_flag = 0;
                        sprintf(message, "{\"method\":\"report\",\"clientToken\":\"wbyq\",\"params\":{\"LEDT0\":0}}");
                        printf("【上行】%s\n", message);
                        MQTT_Publish(TencentCloud_PublishTopic, message, 0);
                        g_Usart3RxCnt = 0;
                        g_Usart3RxFlag = 0;
                    }
                }
                p1 = strstr(message, "LED2");
                if (p1)
                {
                    if ((*(p1 + 6) - '0') == 1)
                    {
                        LED(4,1);
                        led_flag1 = 1;
                        sprintf(message, "{\"method\":\"report\",\"clientToken\":\"wbyq\",\"params\":{\"LEDT1\":1}}");
                        printf("【上行】%s\n", message);
                        MQTT_Publish(TencentCloud_PublishTopic, message, 0);
                        g_Usart3RxCnt = 0;
                        g_Usart3RxFlag = 0;
                    }
                    else
                    {
                        LED(4,0);
                        led_flag1 = 0;
                        sprintf(message, "{\"method\":\"report\",\"clientToken\":\"wbyq\",\"params\":{\"LEDT1\":0}}");
                        printf("【上行】%s\n", message);
                        MQTT_Publish(TencentCloud_PublishTopic, message, 0);
                        g_Usart3RxCnt = 0;
                        g_Usart3RxFlag = 0;
                    }
                }
                p1 = strstr(message, "LEDM");
                if (p1)
                {
                    ledmax = ((*(p1 + 6) - '0')) * 1000 + ((*(p1 + 7) - '0')) * 100 + ((*(p1 + 8) - '0')) * 10 +((*(p1 + 9) - '0'));
                }
            }
      g_Usart3RxFlag = 0;  //清除标志位
      g_Usart3RxCnt = 0;   //恢复下标0起始
    }
    OLED_time(timechar);
    if(!time_flag)//后台计时器
    {
        if(timer1 != sTime.Seconds)
        {
            timers++;
            timer1 = sTime.Seconds;
        }
        if(timers == 60)
        {
            timerm++;
            timers = 0;
        }
        if(timerm == 60)
        {
            timerh++;
            timerm = 0;
        }
        if(timerh == 24)
        {
            timerh = 0;
            BEEP(0);
            HAL_Delay(10);
            BEEP(1);
        }
    }
    LED_SMART(advalue,led_flag);
    OLED_LINE();
    OLED_RefreshGram();
    time ++;
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_LSI|RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.LSIState = RCC_LSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_RTC|RCC_PERIPHCLK_ADC;
  PeriphClkInit.RTCClockSelection = RCC_RTCCLKSOURCE_LSI;
  PeriphClkInit.AdcClockSelection = RCC_ADCPCLK2_DIV6;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
