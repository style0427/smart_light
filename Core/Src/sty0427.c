#include "sty0427.h"
#include "oled.h"
#include <string.h>
extern unsigned char pam40[];
extern unsigned char pm_40x40[]; 
extern unsigned char love20x20[];
extern unsigned char love340x40[];
extern unsigned char fire30x30[];
extern unsigned char clock24[];
extern unsigned char core24[];
extern unsigned char upload24[];
extern unsigned char download24[];
extern unsigned char qq24[];
extern unsigned char umb24[];
extern unsigned char cloud24[];
extern unsigned char ball24[];
extern unsigned char pm_40x40[]; 
//ikun icon
extern unsigned char cxk13x24[];
extern unsigned char cxk113x24[];
extern unsigned char cxk213x24[];
extern unsigned char cxk313x24[];
extern unsigned char cxk413x24[];
extern unsigned char cxk513x24[];
//small icon
extern unsigned char scan1111[];
extern unsigned char shine12x12[];
extern unsigned char msg15x9[];
extern unsigned char cafe9x9[];
extern unsigned char led9x9[];
extern unsigned char RET[];
extern unsigned char tower11x7[];
extern unsigned char wind11x11[];
extern unsigned char win1d11x11[];
extern unsigned char set77[];
extern uint8_t vlan_flag;
extern uint8_t ikunnum;
extern uint16_t ledmax;
//extern uint8_t led_flag;
//extern uint8_t led_flag1;
#define ikunmax 34
uint8_t ikunnum = 0;

//OLED主界面
void OLED_MY(void)
{
    //作者信息
    OLED_DrawChinese(10, 5, 12, 6, 1); //郭
    OLED_DrawChinese(25, 5, 12, 7, 1); //阳
    OLED_DrawString(41, 3, 16, "2020211834", 1);
}
void OLED_main()
{
  OLED_DrawBMP(2,22, 40, 40, (uint8_t *)pam40, 1);
  OLED_DrawLine(51, 29, 120, 29, 1);     //绘制直线
  OLED_DrawChinese(56, 12, 16, 2, 1); //睿
  OLED_DrawChinese(72, 12, 16, 3, 1); //智
  OLED_DrawChinese(87, 12, 16, 4, 1); //路
  OLED_DrawChinese(104, 12, 16, 5, 1); //灯
  OLED_DrawChinese(56, 30, 12, 12, 1); //作
  OLED_DrawChinese(68, 30, 12, 13, 1); //作
  OLED_DrawChinese(80, 30, 12, 14, 1); //作
  OLED_DrawChinese(92, 30, 12, 15, 1); //作
  OLED_DrawChinese(104, 30, 12, 16, 1); //作
}
void ikun(void)
{
    switch(ikunnum)
    {
        case 0:OLED_DrawBMP(57, 20, 13, 24, (uint8_t *)cxk13x24, 1);break;//zuo
        case 1:OLED_DrawBMP(53, 20, 13, 24, (uint8_t *)cxk13x24, 1);break;//zuo
        case 2:OLED_DrawBMP(50, 20, 13, 24, (uint8_t *)cxk13x24, 1);break;//zuo
        case 3:OLED_DrawBMP(50, 20, 16, 24, (uint8_t *)cxk213x24, 1);break;
        case 4:OLED_DrawBMP(50, 20, 16, 24, (uint8_t *)cxk313x24, 1);break;
        case 5:OLED_DrawBMP(50, 20, 16, 24, (uint8_t *)cxk213x24, 1);break;
        case 6:OLED_DrawBMP(50, 20, 16, 24, (uint8_t *)cxk313x24, 1);break;
        case 7:OLED_DrawBMP(53, 20, 13, 24, (uint8_t *)cxk113x24, 1);break;//you
        case 8:OLED_DrawBMP(57, 20, 13, 24, (uint8_t *)cxk13x24, 1);break;//you
        case 9:OLED_DrawBMP(57, 20, 13, 24, (uint8_t *)cxk113x24, 1);break;//you
        case 10:OLED_DrawBMP(57, 20, 13, 24, (uint8_t *)cxk413x24, 1);break;
        case 11:OLED_DrawBMP(57, 20, 13, 24, (uint8_t *)cxk513x24, 1);break;
        case 12:OLED_DrawBMP(57, 20, 13, 24, (uint8_t *)cxk413x24, 1);break;
        case 13:OLED_DrawBMP(57, 20, 13, 24, (uint8_t *)cxk13x24, 1);break;
        case 14:OLED_DrawBMP(57, 20, 13, 24, (uint8_t *)cxk413x24, 1);break;
        case 15:OLED_DrawBMP(57, 20, 13, 24, (uint8_t *)cxk513x24, 1);break;
        case 16:OLED_DrawBMP(57, 20, 13, 24, (uint8_t *)cxk413x24, 1);break;
        case 17:OLED_DrawBMP(57, 20, 13, 24, (uint8_t *)cxk13x24, 1);break;
        case 18:OLED_DrawBMP(61, 20, 13, 24, (uint8_t *)cxk113x24, 1);break;//you
        case 19:OLED_DrawBMP(64, 20, 13, 24, (uint8_t *)cxk113x24, 1);break;//you
        case 20:OLED_DrawBMP(64, 20, 16, 24, (uint8_t *)cxk213x24, 1);break;
        case 21:OLED_DrawBMP(64, 20, 16, 24, (uint8_t *)cxk313x24, 1);break;
        case 22:OLED_DrawBMP(64, 20, 16, 24, (uint8_t *)cxk213x24, 1);break;
        case 23:OLED_DrawBMP(64, 20, 16, 24, (uint8_t *)cxk313x24, 1);break;
        case 24:OLED_DrawBMP(64, 20, 13, 24, (uint8_t *)cxk13x24, 1);break;
        case 25:OLED_DrawBMP(61, 20, 13, 24, (uint8_t *)cxk13x24, 1);break;
        case 26:OLED_DrawBMP(57, 20, 13, 24, (uint8_t *)cxk13x24, 1);break;
        case 27:OLED_DrawBMP(57, 20, 13, 24, (uint8_t *)cxk413x24, 1);break;
        case 28:OLED_DrawBMP(57, 20, 13, 24, (uint8_t *)cxk513x24, 1);break;
        case 29:OLED_DrawBMP(57, 20, 13, 24, (uint8_t *)cxk413x24, 1);break;
        case 30:OLED_DrawBMP(57, 20, 13, 24, (uint8_t *)cxk13x24, 1);break;
        case 31:OLED_DrawBMP(57, 20, 13, 24, (uint8_t *)cxk413x24, 1);break;
        case 32:OLED_DrawBMP(57, 20, 13, 24, (uint8_t *)cxk513x24, 1);break;
        case 33:OLED_DrawBMP(57, 20, 13, 24, (uint8_t *)cxk413x24, 1);break;
        case 34:OLED_DrawBMP(57, 20, 13, 24, (uint8_t *)cxk13x24, 1);break;
    }
    HAL_Delay(100);
}
void OLED_ICON(void)
{
  //底部图标
  OLED_DrawBMP(65, 49, 9, 9, (uint8_t *)cafe9x9, 1);
  OLED_DrawBMP(78, 49, 15, 9, (uint8_t *)msg15x9, 1);
  OLED_DrawBMP(96, 50, 11, 7, (uint8_t *)tower11x7, 1);
  OLED_DrawBMP(111, 49, 14, 9, (uint8_t *)RET, 1);
}
void OLED_LINE(void)
{
  //边界线
  OLED_DrawLine(1, 0, 126, 1, 1);       //绘制直线
  OLED_DrawLine(1, 62, 126, 63, 1);     //绘制直线
  OLED_DrawLine(0, 0, 1, 63, 1);        //绘制直线
  OLED_DrawLine(126, 0, 127, 63, 1);    //绘制直线
}
void fabing(void)
{
               OLED_DrawBMP(3, 10, 40, 40, (uint8_t *)love340x40, 1);
               OLED_DrawBMP(86, 10, 40, 40, (uint8_t *)love340x40, 1);
               OLED_DrawBMP(48, 30, 30, 30, (uint8_t *)fire30x30, 1);
               OLED_DrawChinese(48, 15, 16, 6, 1); //渣
               OLED_DrawChinese(64, 15, 16, 6, 1); //渣
               OLED_DrawChinese(18, 46, 12, 9, 1); //爱
               OLED_DrawChinese(96, 46, 12, 10, 1); //狂
               OLED_DrawChinese(108, 46, 12, 11, 1); //粉
}
void OLED_Chose(const uint8_t i)
{
    OLED_DrawRect(6+(28*i), 17, 35+(28*i), 43, 1);
}
void PAGE_CHOSE(uint8_t page)
{
    uint8_t i = page;
    if(i == 4){i=2;}
    if(i>0)
    {
    OLED_DrawRect(61+(i-1)*16, 47, 77+(i-1)*16, 59, 1);
    }
}
void OLED_KEY(void)
{
  OLED_DrawChinese(17, 46, 12, 0, 1); //按
  OLED_DrawChinese(33, 46, 12, 1, 1); //键
  OLED_DrawChinese(49, 46, 12, 2, 1); //开
  OLED_DrawChinese(65, 46, 12, 3, 1); //始
  OLED_DrawChinese(81, 46, 12, 4, 1); //操
  OLED_DrawChinese(97, 46, 12, 5, 1); //作
}
void LED_SMART(uint16_t advalue,uint8_t led_flag)
{  
        if(advalue>=(ledmax*0.8))//低光照开灯
        {   
            if(!led_flag1)
            {LED(4,1);}
            if(advalue>=(ledmax)&&!led_flag){LED(3,1);}
            OLED_DrawBMP(50, 49, 9, 9, (uint8_t *)led9x9, 1);
            //printf("/*当前环境较暗，探照灯已开启*/\n");
        }
        else//光照弱时开灯
        {
            if(!led_flag){LED(3,0);}
            if(!led_flag1){LED(4,0);}
            //printf("/*探照灯已关闭*/\n");
        }
}
void OLED_PAGE(uint8_t page)
{
    switch(page)
    {
        case 0:OLED_main();
               break;
        case 1:ikun();
               if(ikunnum==ikunmax){ikunnum = 0;}
               else {ikunnum++;}
               break;
        case 2:OLED_DrawBMP(0,22, 40, 40, (uint8_t *)pm_40x40, 1);
               break;
        case 3:OLED_DrawBMP(9, 18, 24, 24, (uint8_t *)clock24, 1);
               OLED_DrawBMP(37, 18, 24, 24, (uint8_t *)core24, 1);
               OLED_DrawBMP(65, 18, 24, 24, (uint8_t *)upload24, 1);
               OLED_DrawBMP(93, 18, 24, 24, (uint8_t *)download24, 1);
               if(vlan_flag)
               {
                   OLED_DrawBMP(5, 48, 11, 11, (uint8_t *)wind11x11, 1);
               }
               else
               {
                   OLED_DrawBMP(5, 48, 11, 11, (uint8_t *)win1d11x11, 1);
               }
               break;
        case 4:OLED_DrawBMP(5 , 51, 7, 7, (uint8_t *)set77, 1);
               OLED_DrawBMP(9 , 18, 24, 24, (uint8_t *)umb24, 1);
               OLED_DrawBMP(37, 18, 24, 24, (uint8_t *)qq24, 1);
               OLED_DrawBMP(65, 18, 24, 24, (uint8_t *)cloud24, 1);
               OLED_DrawBMP(93, 18, 24, 24, (uint8_t *)ball24, 1);
               break;
    }
    PAGE_CHOSE(page);
}
void OLED_WAIT(char *string,uint8_t i)
{
    uint16_t length = strlen(string) * (i/2);
    OLED_ResetGram();
    OLED_DrawString ((120-length)/2,(22-i/2),i,string,1);
    OLED_DrawBMP(57,30, 11, 11, (uint8_t *)scan1111, 1);
    OLED_Get();
    OLED_RefreshGram();
}
