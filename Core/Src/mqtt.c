#include "mqtt.h"
#include "usart.h"
#include <stdio.h>
#include <string.h>
#include "main.h"

static uint8_t mqtt_txBufLen;
static uint8_t mqtt_rxBufLen;
static uint8_t mqtt_txBuf[256];
static uint8_t mqtt_rxBuf[256];

//连接成功服务器回应 20 02 00 00
//客户端主动断开连接 e0 00
static uint8_t PACKET_CONNECTACK[]    = {0x20, 0x02, 0x00, 0x00}; //确认连接请求
static uint8_t PACKET_SUBACK[] = {0x90, 0x03};               //订阅/取消订阅确认
static uint8_t PACKET_DISCONNECT[] = {0xE0, 0x00};           //断开连接
static uint8_t PACKET_PINGREQ[]  = {0xC0, 0x00};             //心跳请求
static uint8_t PACKET_PINGRESP[] = {0xD0, 0x00};             //心跳响应

//串口3发送数据
static void MQTT_SendData(uint8_t *buf, uint16_t len)
{
    HAL_UART_Transmit(&huart3, buf, len, 0xFFFF);
}

//毫秒延时
static void MQTT_Delay(uint32_t ms)
{
    HAL_Delay(ms);
}

/******************************************************************************\
 * 函数功能：向服务端发送控制报文
 * 形参说明：
 *          [in]buf - 控制报文
 *          [in]len - 报文长度
 *          [in]response - 正确的报文响应值
 * 返 回 值：0 - 成功 / 1 - 失败
\******************************************************************************/
static uint8_t MQTT_SendControlPackets(uint8_t *buf, uint8_t len, uint8_t *response)
{
    uint16_t i, j;
    g_Usart3RxCnt = 0;
    g_Usart3RxFlag = 0;
    for (i = 0; i < 5; i++)
    {
        mqtt_rxBufLen = 0;
        memset(mqtt_rxBuf, 0, sizeof(mqtt_rxBuf));
        MQTT_SendData(buf, len);
        for (j = 0; j < 1000 * 3; j++)
        {
            if (g_Usart3RxFlag)
            {
                memcpy(mqtt_rxBuf, g_Usart3RxBuf, g_Usart3RxCnt);
                //////////////////////////////////////////////////////
#if 0
                printf("server response:[ ");
                for (uint8_t k = 0; k < g_ucUsart3RxCnt; k++)
                    printf("%02x ", g_ucUsart3RxBuf[k]);
                printf("]\r\n");
#endif
                //////////////////////////////////////////////////////
                g_Usart3RxCnt = 0;
                g_Usart3RxFlag = 0;
                if ((mqtt_rxBuf[0] == response[0]) && (mqtt_rxBuf[1] == response[1]))
                {
                    printf("[OK]\n");
                    return 0;
                }
            }
            MQTT_Delay(1);
        }
    }
    printf("[ERROR]\n");
    return 1;
}

/******************************************************************************\
 * 函数功能：与服务端建立MQTT连接
 * 形参说明：
 *          [in]clientId - 客户端id
 *          [in]userName - 用户名
 *          [in]passWord - 密码
 * 返 回 值：0 - 成功 / 1 - 失败
\******************************************************************************/
uint8_t MQTT_Connect(char *clientId, char *userName, char *passWord)
{
    uint32_t RemainingLength = 0;
    uint32_t clientIdLen = strlen(clientId);
    uint32_t userNameLen = strlen(userName);
    uint32_t passWordLen = strlen(passWord);
    mqtt_txBufLen = 0;
    memset(mqtt_txBuf, 0, sizeof(mqtt_txBuf));
    //【1,固定报头】
    // 1.1 [7:4]报文类型 [3:0]报文标志
    mqtt_txBuf[mqtt_txBufLen++] = (T_CONNECT << 4) | F_CONNECT;
    // 1.2 整个报文剩余长度：可变报头+有效载荷
    RemainingLength = 10 + (2 + clientIdLen) + (2 + userNameLen) + (2 + passWordLen);
    do
    {
        uint8_t encodedByte = RemainingLength % 128;
        RemainingLength = RemainingLength / 128;
        // if there are more data to encode, set the top bit of this byte
        if (RemainingLength > 0)
            encodedByte = encodedByte | 128;
        mqtt_txBuf[mqtt_txBufLen++] = encodedByte;
    }
    while (RemainingLength > 0);
    //【2,可变报头】
    // 2.1 协议名长度
    mqtt_txBuf[mqtt_txBufLen++] = 0x00; //协议名长度MSB
    mqtt_txBuf[mqtt_txBufLen++] = 0x04; //协议名长度LSB
    // 2,2 协议名
    mqtt_txBuf[mqtt_txBufLen++] = 'M';
    mqtt_txBuf[mqtt_txBufLen++] = 'Q';
    mqtt_txBuf[mqtt_txBufLen++] = 'T';
    mqtt_txBuf[mqtt_txBufLen++] = 'T';
    // 2.3 协议级别
    mqtt_txBuf[mqtt_txBufLen++] = 0x04;
    // 2.4 连接标志
    mqtt_txBuf[mqtt_txBufLen++] = 0xC2; //用户名=1,密码=1,清理会话=1
    // 2.5 保持连接时间
    mqtt_txBuf[mqtt_txBufLen++] = 0;    //时间MSB
    mqtt_txBuf[mqtt_txBufLen++] = 100;  //时间LSB,100s
    //【3,有效载荷】
    // 3.1 客户端标识符(一定有)
    mqtt_txBuf[mqtt_txBufLen++] = clientIdLen >> 8;   //客户端标识符MSB
    mqtt_txBuf[mqtt_txBufLen++] = clientIdLen;        //客户端标识符LSB
    memcpy(&mqtt_txBuf[mqtt_txBufLen], clientId, clientIdLen); //拷贝标识符长度
    mqtt_txBufLen += clientIdLen;
    // 3.2 遗嘱主题
    // 3.3 遗嘱消息
    // 3.4 用户名
    if (userNameLen > 0)
    {
        mqtt_txBuf[mqtt_txBufLen++] = userNameLen >> 8; //用户名MSB
        mqtt_txBuf[mqtt_txBufLen++] = userNameLen;      //用户名LSB
        memcpy(&mqtt_txBuf[mqtt_txBufLen], userName, userNameLen); //拷贝标识符长度
        mqtt_txBufLen += userNameLen;
    }
    // 3.5 密码
    if (passWordLen > 0)
    {
        mqtt_txBuf[mqtt_txBufLen++] = passWordLen >> 8; //密码MSB
        mqtt_txBuf[mqtt_txBufLen++] = passWordLen;      //密码LSB
        memcpy(&mqtt_txBuf[mqtt_txBufLen], passWord, passWordLen); //拷贝标识符长度
        mqtt_txBufLen += passWordLen;
    }
    return MQTT_SendControlPackets(mqtt_txBuf, mqtt_txBufLen, PACKET_CONNECTACK);
}

/******************************************************************************\
 * 函数功能：发布 消息
 * 形参说明：
 *          [in]topic   - 主题名
 *          [in]message - 要推送的信息
 *          [in]Qos     - 服务质量等级
 * 返 回 值：报文长度
\******************************************************************************/
uint8_t MQTT_Publish(char *topic, char *message, uint8_t Qos)
{
    uint32_t RemainingLength = 0;
    uint16_t topicLen = strlen(topic);
    uint32_t messageLen = strlen(message);
    uint16_t id = 0;
    mqtt_txBufLen = 0;
    memset(mqtt_txBuf, 0, sizeof(mqtt_txBuf));
    //【1,固定报头】
    // 1.1 [7:4]报文类型 [3:0]报文标志
    mqtt_txBuf[mqtt_txBufLen++] = (T_PUBLISH << 4) | F_PUBLISH;
    // 1.2 整个报文剩余长度：可变报头(主题名 + 报文标识符) + 有效载荷
    RemainingLength = (2 + topicLen) + (Qos ? 2 : 0) + messageLen;
    do
    {
        uint8_t encodedByte = RemainingLength % 128;
        RemainingLength = RemainingLength / 128;
        // if there are more data to encode, set the top bit of this byte
        if (RemainingLength > 0)
            encodedByte = encodedByte | 128;
        mqtt_txBuf[mqtt_txBufLen++] = encodedByte;
    }
    while (RemainingLength > 0);
    //【2,可变报头】
    // 2.1 主题名长度+主题名
    mqtt_txBuf[mqtt_txBufLen++] = topicLen >> 8;  //主题名长度MSB
    mqtt_txBuf[mqtt_txBufLen++] = topicLen;       //主题名长度LSB
    memcpy(&mqtt_txBuf[mqtt_txBufLen], topic, topicLen); //主题名
    mqtt_txBufLen += topicLen;
    // 2.2 报文标识符(Qos>0时才存在)
    if (Qos)
    {
        mqtt_txBuf[mqtt_txBufLen++] = F_PUBLISH >> 8; //报文标识符MSB
        mqtt_txBuf[mqtt_txBufLen++] = F_PUBLISH;      //报文标识符LSB
        id++;
    }
    //【3,有效载荷】
    memcpy(&mqtt_txBuf[mqtt_txBufLen], message, messageLen);
    mqtt_txBufLen += messageLen;
    //【4,响应】
    //【5,动作】
    //发送控制报文
    MQTT_SendData(mqtt_txBuf, mqtt_txBufLen);
    return mqtt_txBufLen;
}

/******************************************************************************\
 * 函数功能：订阅/取消订阅 主题
 * 形参说明：
 *           [in]topic - 主题名
 *           [in]stat  - 状态(1-订阅,0-取消订阅)
 *           [in]Qos   - 服务质量等级
 * 返 回 值：0 - 成功 / 1 - 失败
\******************************************************************************/
uint8_t MQTT_Subscribe(char *topic, uint8_t stat, uint8_t Qos)
{
    uint8_t identifier;                //---报文标识符
    uint32_t RemainingLength = 0;      //---剩余长度
    uint32_t topicLen = strlen(topic); //---主题长度
    mqtt_txBufLen = 0;
    memset(mqtt_txBuf, 0, sizeof(mqtt_txBuf));
    //【1,固定报头】
    // 1.1 [7:4]报文类型 [3:0]报文标志
    if (stat) //---订阅模式
        mqtt_txBuf[mqtt_txBufLen++] = (T_SUBSCRIBE << 4) | F_SUBSCRIBE;
    else //---取消订阅模式
        mqtt_txBuf[mqtt_txBufLen++] = (T_UNSUBSCRIBE << 4) | F_UNSUBSCRIBE;
    // 1.2 整个报文剩余长度：可变报头(报文标识符) + 有效载荷(主题过滤器)
    RemainingLength = 2 + (2 + topicLen) + (stat ? 1 : 0);
    do
    {
        uint32_t encodedByte = RemainingLength % 128;
        RemainingLength = RemainingLength / 128;
        // if there are more data to encode, set the top bit of this byte
        if (RemainingLength > 0)
            encodedByte = encodedByte | 128;
        mqtt_txBuf[mqtt_txBufLen++] = encodedByte;
    }
    while (RemainingLength > 0);
    //【2,可变报头】
    // 2.1 报文标识符 --- 订阅/取消订阅主题 使用的报文标识符要一致
    if (stat)    identifier = F_SUBSCRIBE;
    else identifier = F_UNSUBSCRIBE;
    mqtt_txBuf[mqtt_txBufLen++] = identifier >> 8; //报文标识符MSB
    mqtt_txBuf[mqtt_txBufLen++] = identifier;      //报文标识符LSB
    //【3,有效载荷】
    // 3.1 主题过滤器
    mqtt_txBuf[mqtt_txBufLen++] = topicLen >> 8;   //主题长度MSB
    mqtt_txBuf[mqtt_txBufLen++] = topicLen;        //主题长度LSB
    memcpy(&mqtt_txBuf[mqtt_txBufLen], topic, topicLen); //拷贝主题
    mqtt_txBufLen += topicLen;
    // 3.2 服务质量等级 --- 订阅时才存在
    if (stat)
        mqtt_txBuf[mqtt_txBufLen++] = Qos;
    //发送控制报文
    return MQTT_SendControlPackets(mqtt_txBuf, mqtt_txBufLen, PACKET_SUBACK);
}

/******************************************************************************\
 * 函数功能：发送心跳包以保持网络连接（100s内要不断重发）
 * 形参说明：None
 * 返 回 值：None
\******************************************************************************/
uint8_t MQTT_PingReqPer100s(void)
{
    return MQTT_SendControlPackets(PACKET_PINGREQ, sizeof(PACKET_PINGREQ), PACKET_PINGRESP);
}

/******************************************************************************\
 * 函数功能：与服务端主动断开MQTT连接
 * 形参说明：None
 * 返 回 值：None
\******************************************************************************/
void MQTT_DisConnect(void)
{
    MQTT_SendData(PACKET_DISCONNECT, sizeof(PACKET_DISCONNECT));
}

