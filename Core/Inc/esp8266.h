#ifndef __ESP8266_H_
#define __ESP8266_H_

#include "main.h"
extern uint8_t vlan_flag;
typedef struct
{
    char ssid[20];
    char pwd[20];
    char ip[20];
    unsigned short port;
} ESP8266_InfoTypeDef;

uint8_t ESP8266_STA_TCPClientInit(ESP8266_InfoTypeDef espInfo);
uint8_t ESP8266_ReqRes_DataParsing(char *reqRes, char *keywords, char *result);
#endif
