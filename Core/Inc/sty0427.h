#ifndef __STY0427_H_
#define __STY0427_H_

#include "main.h"
extern uint8_t ikunnum;
void OLED_MY(void);
void OLED_main(void);
void ikun(void);
void OLED_ICON(void);
void OLED_LINE(void);
void OLED_Chose(const uint8_t i);
void PAGE_CHOSE(uint8_t page);
void LED_SMART(uint16_t advalue,uint8_t led_flag);
void OLED_KEY(void);
void OLED_PAGE(uint8_t page);
void OLED_WAIT(char *string,uint8_t i);
#endif
