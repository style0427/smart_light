/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f1xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
//LED(x,y)用于控制LED显示,其中x为灯序号，y为亮灭，1亮0灭
#define LED(x,y) if(y){switch(x){case 1:HAL_GPIO_WritePin(GPIOB,GPIO_PIN_6,GPIO_PIN_RESET);break;case 2:HAL_GPIO_WritePin(GPIOB,GPIO_PIN_7,GPIO_PIN_RESET);break;case 3:HAL_GPIO_WritePin(GPIOB,GPIO_PIN_8,GPIO_PIN_RESET);break;case 4:HAL_GPIO_WritePin(GPIOB,GPIO_PIN_9,GPIO_PIN_RESET);}}else{switch(x){case 1:HAL_GPIO_WritePin(GPIOB,GPIO_PIN_6,GPIO_PIN_SET);break;case 2:HAL_GPIO_WritePin(GPIOB,GPIO_PIN_7,GPIO_PIN_SET);break;case 3:HAL_GPIO_WritePin(GPIOB,GPIO_PIN_8,GPIO_PIN_SET);break;case 4:HAL_GPIO_WritePin(GPIOB,GPIO_PIN_9,GPIO_PIN_SET);break;}}
#define BEEP(x) if(x){HAL_GPIO_WritePin(GPIOA,GPIO_PIN_6,GPIO_PIN_RESET);}else{HAL_GPIO_WritePin(GPIOA,GPIO_PIN_6,GPIO_PIN_SET);}
#define KEY_1   HAL_GPIO_ReadPin(GPIOA,GPIO_PIN_0)
#define KEY_2   HAL_GPIO_ReadPin(GPIOA,GPIO_PIN_1)
#define KEY_3   HAL_GPIO_ReadPin(GPIOA,GPIO_PIN_4)
#define KEY_4   HAL_GPIO_ReadPin(GPIOA,GPIO_PIN_5)
void OLED_Get(void);
/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */
extern void HAL_Delayus(uint16_t us);
extern uint8_t led_flag;
extern uint8_t led_flag1;
/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define KEY0_Pin GPIO_PIN_0
#define KEY0_GPIO_Port GPIOA
#define KEY1_Pin GPIO_PIN_1
#define KEY1_GPIO_Port GPIOA
#define KEY2_Pin GPIO_PIN_4
#define KEY2_GPIO_Port GPIOA
#define KEY3_Pin GPIO_PIN_5
#define KEY3_GPIO_Port GPIOA
#define BEE_Pin GPIO_PIN_6
#define BEE_GPIO_Port GPIOA
#define LED1_Pin GPIO_PIN_6
#define LED1_GPIO_Port GPIOB
#define LED2_Pin GPIO_PIN_7
#define LED2_GPIO_Port GPIOB
#define LED3_Pin GPIO_PIN_8
#define LED3_GPIO_Port GPIOB
#define LED4_Pin GPIO_PIN_9
#define LED4_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */
