#ifndef __DS18B20_H_
#define __DS18B20_H_

#include "main.h"
#include "gpio.h"

uint8_t DS18B20_Init(void);
float DS18B20_GetTemp(void);

#endif
