#ifndef __OLED_H_ 
#define __OLED_H_

#include "main.h"

/* 启用GRAM模式 */
#define OLED_GRAM_MODE

/* 定义屏幕尺寸 */
#define OLED_WIDTH  128
#define OLED_HEIGHT 64

//////////////////////////////////////////////////////////////////////////////////////////////////
// 公共基础部分
void OLED_Init(void);
void OLED_Cls(void);
///////////////////////////////////////////////////////////////////////////////////////////////////
//常规用法
#ifndef OLED_GRAM_MODE
void OLED_DispChar(uint8_t _Page, uint8_t _Col, uint16_t _Width, uint8_t _Height, uint8_t _ucChr, uint8_t _Mode);
void OLED_DispString(uint8_t _Page, uint8_t _Col, uint8_t _Height, uint8_t *_pstr, uint8_t _Mode);
void OLED_DispChinese(uint8_t _Page, uint8_t _Col, uint8_t _Height, uint8_t _Number, uint8_t _Mode);
void OLED_DispBMP(uint8_t _Page, uint8_t _ucCol, uint8_t _Width, uint8_t _Height, uint8_t *_pbuf, uint8_t _Mode);
#else
void OLED_ResetGram(void);
void OLED_RefreshGram(void);
void OLED_DrawPoint(uint16_t _X, uint16_t _Y, uint16_t _Color);
void OLED_DrawChar(uint16_t _X, uint16_t _Y, uint16_t _Width, uint16_t _Height, uint8_t _Chr, uint8_t _Mode);
void OLED_DrawString(uint16_t _X, uint16_t _Y, uint16_t _Height, char *_pstr, uint8_t _Mode);
void OLED_DrawChinese(uint16_t _X, uint16_t _Y, uint16_t _Height, uint8_t _Number, uint8_t _Mode);
void OLED_DrawBMP(uint16_t _X, uint16_t _Y, uint16_t _Width, uint16_t _Height, uint8_t *_pbuf, uint8_t _Mode);
void OLED_DrawLine(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2, uint16_t c);
void OLED_DrawAngleLine(uint16_t x, uint16_t y, float angle, uint32_t start, uint32_t len, uint16_t c);
void OLED_DrawCircle(uint16_t x0, uint16_t y0, uint8_t r, uint16_t c);
void OLED_DrawRect(uint16_t sx, uint16_t sy, uint16_t ex, uint16_t ey, uint16_t c);
void OLED_FillRect(uint16_t sx, uint16_t sy, uint16_t ex, uint16_t ey, uint16_t c);
#endif

#endif
